library harish_package;

import 'package:flutter/material.dart';


 class  ScreenDetails extends StatefulWidget {
   String title;

  ScreenDetails({Key key, @required this.title}) : super(key: key);
  @override
  _State createState() => _State();
  /// Bu şekilde döküman yorumları oluşturabilirsiniz kullanan kişiler için faydalı olur.

}

class _State extends State<ScreenDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container(child: Center(child: Text(widget.title!=null ? widget.title : "Text")),));
  }
}


