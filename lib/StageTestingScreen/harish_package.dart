library harish_package;

import 'package:flutter/material.dart';
import 'package:harish_package/Constants/AppColors.dart';

import 'custom_alert_box.dart';



class CustomAlertBox {
  /// Bu şekilde döküman yorumları oluşturabilirsiniz kullanan kişiler için faydalı olur.
  static Future showCustomAlertBox({
    @required BuildContext context,
    @required Widget enterWidget,
    @required Color color,
  }) {
    assert(context != null, "context is null!!");
    assert(enterWidget != null, "willDisplayWidget is null!!");

    return showDialog(
        context: context,

        builder: (context) {
          return AlertDialog(

            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15)),
            ),
            backgroundColor: color!= null ? color : AppColors.APP_COLOR_PRIMARY,
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                enterWidget,
                MaterialButton(
                  color:AppColors.APP_COLOR_WHITE,
                  child: Text('close alert'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            ),
            elevation: 10,
          );
        });
  }


  Widget get name{

    return ScreenDetails(title:"Test");



  }
}