import 'package:cyclop/cyclop.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Third.dart';

class Second extends StatefulWidget {

  Function() onPress;

  Second({Key key, this.onPress}) : super(key: key);
  @override
  _SecondState createState() => _SecondState();
}

class _SecondState extends State<Second> {
  Color appbarColor = Colors.blueGrey;

  Color backgroundColor = Colors.grey.shade200;

  Set<Color> swatches = Colors.primaries.map((e) => Color(e.value)).toSet();

  @override
  void initState(){
    super.initState();

    getStringValuesSF();

  }

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    String stringValue = prefs.getString('stringValue');
    print("Init Value====>" + stringValue.toString());
    int colorCodeInt = int.parse(stringValue);
    setState(() {
      backgroundColor = Color(colorCodeInt);
      this.mounted;
    });

    print("BackGroundColor====>" + backgroundColor.toString());
    return stringValue;
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final bodyTextColor =
        ThemeData.estimateBrightnessForColor(backgroundColor) == Brightness.dark
            ? Colors.white70
            : Colors.black87;

    final appbarTextColor =
        ThemeData.estimateBrightnessForColor(backgroundColor) == Brightness.dark
            ? Colors.black
            : Colors.white;





    return Scaffold(
      backgroundColor: backgroundColor!=null?backgroundColor:Colors.white,
      appBar: AppBar(
        title: Text('Cyclop Demo',
          style: textTheme.headline6.copyWith(color: appbarTextColor),),
        backgroundColor: bodyTextColor,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context, true);
            },
            icon: Icon(Icons.arrow_back_ios_rounded,color: appbarTextColor,)),

      ),
      body: Container(
        padding: const EdgeInsets.all(12),
        child: Center(
          child: Column(
            children: [
              Text(
                'Select the background & appbar colors',
                style: textTheme.headline6.copyWith(color: bodyTextColor),
              ),
              _buildButtons(),
              if (!kIsWeb) Center(child: Image.asset('assets/images/visible.png',color: bodyTextColor,)),
              RaisedButton(
                child: Text("3rd",),
                onPressed: (){
                  /*Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Second()),
                );*/
                  this.mounted;
                  getStringValuesSF();
                  widget.onPress();

               /*   Navigator.of(context).push(new MaterialPageRoute(builder: (_)=>new Third()),)
                      .then((val)=>val?getStringValuesSF():null);*/
                },),
            ],
          ),
        ),
      ),
    );
  }



  Expanded _buildButtons() {

    final textTheme = Theme.of(context).textTheme;

    final bodyTextColor =
    ThemeData.estimateBrightnessForColor(backgroundColor) == Brightness.dark
        ? Colors.white70
        : Colors.black87;

    final appbarTextColor =
    ThemeData.estimateBrightnessForColor(appbarColor) == Brightness.dark
        ? Colors.white70
        : Colors.black87;

    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Center(
            child: Text(
              "Primary",
              style: textTheme.headline6.copyWith(color: bodyTextColor),
            ),
          ),
          Center(
            child: ColorButton(

              darkMode: false,
              key: Key('c1'),
              color: backgroundColor,
              swatches: swatches,
              /*onColorChanged: (value) => setState(
                    () => backgroundColor = value,
              ),*/
              onColorChanged: (value) {
                setState(() {
                  backgroundColor = value;
                  print("Background Value Color====" +
                      backgroundColor.toString());

                  setPrefs(value);
                });
              },
              onSwatchesChanged: (newSwatches) =>
                  setState(() => swatches = newSwatches),
            ),
          ),
        ],
      ),
    );
  }

  void setPrefs(Color value) async {
    var parts = value.toString().split('(');
    var date = parts.sublist(1).join('(').trim();
    List<String> c = date.split("");
    c.removeLast();
    String colorCode = c.join();
    print(c.join());

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('stringValue', colorCode.toString());
  }
}
