import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Third extends StatefulWidget {
  @override
  _ThirdState createState() => _ThirdState();
}

class _ThirdState extends State<Third> {
  Color appbarColor = Colors.blueGrey;

  Color backgroundColor = Colors.grey.shade200;

  Set<Color> swatches = Colors.primaries.map((e) => Color(e.value)).toSet();

  @override
  void initState() {
    super.initState();

    getStringValuesSF();
  }

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    String stringValue = prefs.getString('stringValue');
    print("Init Value====>" + stringValue.toString());
    int colorCodeInt = int.parse(stringValue);
    setState(() {
      backgroundColor = Color(colorCodeInt);
      this.mounted;
    });

    print("BackGroundColor====>" + backgroundColor.toString());
    return stringValue;
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final bodyTextColor =
    ThemeData.estimateBrightnessForColor(backgroundColor) == Brightness.dark
        ? Colors.white70
        : Colors.black87;

    final appbarTextColor =
    ThemeData.estimateBrightnessForColor(backgroundColor) == Brightness.dark
        ? Colors.black
        : Colors.white;

    return Scaffold(
      backgroundColor: backgroundColor!=null?backgroundColor:Colors.white,
      appBar: AppBar(
        title: Text('Cyclop Demo',
          style: textTheme.headline6.copyWith(color: appbarTextColor),),
        backgroundColor: bodyTextColor,
        leading: IconButton(
            onPressed: () {
              Navigator.pop(context, true);
            },
            icon: Icon(Icons.arrow_back_ios_rounded,color: appbarTextColor,)),

      ),
      body: Container(
        padding: const EdgeInsets.all(12),
        child: Center(
          child: Column(
            children: [
              Text(
                'Select the background & appbar colors',
                style: textTheme.headline6.copyWith(color: bodyTextColor),
              ),
              //  _buildButtons(),

              RaisedButton(
                onPressed: () async {


                  final pref = await SharedPreferences.getInstance();

                  this.mounted;
                  await pref.clear();
                },
                child: Text("Clear All Value"),
              ),
              if (!kIsWeb) Center(child: Image.asset('assets/images/visible.png',color: bodyTextColor,)),
            ],
          ),
        ),
      ),
    );
  }
}
