import 'dart:math';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_svg/svg.dart';
import 'package:toast/toast.dart';

import 'Constants/AppColors.dart';
import 'Constants/AppStrings.dart';
import 'Constants/SvgPictures.dart';
import 'Constants/fontFamily.dart';
import 'Constants/fontSize.dart';
import 'Utils/SystemUtils.dart';

class SignUpScreenComponent extends StatefulWidget {
  final Function() facebookSignUp;
  final Function() googleSignUp;
  final Function() phoneNumberSignInFunction;
  final Function() emailPasswordSignInFunction;
  final Function() signInFunction;
  final Function() colorPicker;
  final Function(CountryCode c) countryCodeSelection;
  final TextEditingController phoneNumberController;
  final TextEditingController emailController;
  final TextEditingController passwordController;
  final TextEditingController firstNameController;
  final TextEditingController lastNameController;
  final GlobalKey globalKey;
  final GlobalKey phoneGlobalKey;
  Color themeColor;
  final Color buttonColor;
  final Color containerPrimaryColor;
  final String signUpScreenText;
  final List<String> tabEnabledName;

  SignUpScreenComponent({
    Key key,
    @required this.facebookSignUp,
    @required this.googleSignUp,
    @required this.phoneNumberSignInFunction,
    @required this.phoneNumberController,
    @required this.globalKey,
    @required this.emailPasswordSignInFunction,
    @required this.signInFunction,
    @required this.emailController,
    @required this.colorPicker,
    @required this.passwordController,
    @required this.firstNameController,
    @required this.lastNameController,
    @required this.phoneGlobalKey,
    @required this.themeColor,
    @required this.buttonColor,
    @required this.containerPrimaryColor,
    @required this.signUpScreenText,
    @required this.tabEnabledName,
    @required this.countryCodeSelection,
  }) : super(key: key);

  @override
  _SignUpScreenComponentState createState() => _SignUpScreenComponentState();
}

class _SignUpScreenComponentState extends State<SignUpScreenComponent> {
  bool _obscureText = false;
  bool _passwordVisible = false;

  String phoneNumber = "";

  String initalSelection = 'IN';

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
      _passwordVisible = !_passwordVisible;
    });
  }

  bool lightTheme = true;
  Color currentColor = Colors.limeAccent;
  List<Color> currentColors = [Colors.limeAccent, Colors.green];

  void changeColor(Color color) => setState(() => currentColor = color);
  void changeColors(List<Color> colors) => setState(() => currentColors = colors);
  @override
  void initState(){
    super.initState();

    getStringValuesSF();

  }

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    String stringValue = prefs.getString('stringValue');
    print("Init Value====>" + stringValue.toString());
    int colorCodeInt = int.parse(stringValue);
    setState(() {
      widget.themeColor = Color(colorCodeInt);
      this.mounted;
    });

    print("BackGroundColor12====>" + widget.themeColor.toString());
    return stringValue;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: widget.themeColor != null
            ? widget.themeColor
            : AppColors.APP_COLOR_PRIMARY,
        body: ListView(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: 60.0, left: 30.0, right: 30.0, bottom: 30.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Center(
                    child: Text(
                      widget.signUpScreenText != null
                          ? widget.signUpScreenText
                          : AppStrings.LOGO,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 50.0,
                          fontFamily: FontFamily.Gilroy),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            DefaultTabController(
                length: widget.tabEnabledName.length,
                child: Column(
                  children: [
                    TabBar(
                      indicatorColor: AppColors.APP_COLOR_WHITE,
                      unselectedLabelColor: AppColors.APP_GRAY_1,
                      labelColor: AppColors.APP_COLOR_WHITE,
                      isScrollable: true,
                      tabs: List<Widget>.generate(widget.tabEnabledName.length,
                          (int index) {
                        return Container(
                            width: MediaQuery.of(context).size.width * 0.25,
                            child: new Tab(text: widget.tabEnabledName[index]));
                      }),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: TabBarView(
                        children: List<Widget>.generate(
                            widget.tabEnabledName.length, (int index) {
                          if (widget.tabEnabledName[index] ==
                              AppStrings.SOCIAL) {
                            return _socialWidget;
                          } else if (widget.tabEnabledName[index] ==
                              AppStrings.MOBILE) {
                            return _mobileWidget;
                          } else if (widget.tabEnabledName[index] ==
                              AppStrings.EMAIL) {
                            return _emailWidget;
                          } else {
                            return Container();
                          }
                        }),
                      ),
                    ),
                  ],
                )),
          ],
        ));
  }

  Widget get _socialWidget {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5.0),
            topRight: Radius.circular(5.0),
          ),
        ),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(20),
              child: Text(
                AppStrings.SOCIAL_LOGIN,
                style: TextStyle(
                  fontFamily: FontFamily.Gilroy,
                  fontSize: FontSize.FONT_20,
                  color: AppColors.APP_COLOR_PRIMARY,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              width: 40,
              height: 10,
            ),
            Container(
              padding: EdgeInsets.all(20),
              child: Text(
                AppStrings.QUICK_SIGN_IN_SOCIAL,
                style: TextStyle(
                  fontFamily: FontFamily.Gilroy,
                  fontSize: FontSize.FONT_13,
                  color: AppColors.APP_GRAY_1,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              width: 40,
              height: 10,
            ),
            InkWell(
              onTap: () {
                widget.googleSignUp();
              },
              child: Card(
                elevation: 10,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: 48,
                  padding: EdgeInsets.all(6),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        SvgPictures.GOOGLE_SVG,
                        height: 40,
                        width: 40,
                      ),
                      SizedBox(width: 12),
                      Text(
                        AppStrings.SIGN_IN_WITH_GOOGLE,
                        style: TextStyle(
                            fontSize: FontSize.FONT_13,
                            fontWeight: FontWeight.bold,
                            fontFamily: FontFamily.Gilroy,
                            color: AppColors.APP_COLOR_PRIMARY),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 30,
              height: 10,
            ),
            InkWell(
              onTap: () {
                widget.facebookSignUp();
              },
              child: Card(
                color: AppColors.FACEBOOK_COLOR,
                elevation: 10,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: 48,
                  padding: EdgeInsets.all(6),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        SvgPictures.FB_SVG,
                        height: 40,
                        width: 40,
                      ),
                      SizedBox(width: 12),
                      Text(
                        AppStrings.SIGN_IN_WITH_FACEBOOK,
                        style: TextStyle(
                            fontSize: FontSize.FONT_13,
                            fontWeight: FontWeight.bold,
                            fontFamily: FontFamily.Gilroy,
                            color: AppColors.APP_COLOR_WHITE),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 30,
              height: 10,
            ),
            InkWell(
              onTap: () {
                getStringValuesSF();
                widget.colorPicker();
              },
              child: Card(
                color: AppColors.FACEBOOK_COLOR,
                elevation: 10,
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.6,
                  height: 48,
                  padding: EdgeInsets.all(6),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        SvgPictures.FB_SVG,
                        height: 40,
                        width: 40,
                      ),
                      SizedBox(width: 12),
                      Text(
                        "Color Picker",
                        style: TextStyle(
                            fontSize: FontSize.FONT_13,
                            fontWeight: FontWeight.bold,
                            fontFamily: FontFamily.Gilroy,
                            color: AppColors.APP_COLOR_WHITE),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get _mobileWidget {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5.0),
            topRight: Radius.circular(5.0),
          ),
        ),
        child: Form(
          key: widget.phoneGlobalKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: Text(
                  AppStrings.SIGN_IN,
                  style: TextStyle(
                    fontFamily: FontFamily.Gilroy,
                    fontSize: FontSize.FONT_20,
                    color: AppColors.APP_COLOR_PRIMARY,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              TextFormField(
                autofocus: true,
                onChanged: (val) {
                  setState(() {
                    if (val.length == 10) {
                      SystemUtils.hideKeyboard(context);
                    }
                  });
                },
                controller: widget.phoneNumberController,
                decoration: new InputDecoration(
                  prefix: Container(
                    margin: EdgeInsets.only(right: 20),
                    decoration: new BoxDecoration(
                      border: Border.all(
                        color: AppColors.APP_COLOR_WHITE,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: AppColors.halfWhite,
                    ),
                    height: 30,
                    width: 50,
                    child: CountryCodePicker(
                      showFlagDialog: true,
                      onChanged: _onCountryChange,
                      // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                      initialSelection: initalSelection,
                      favorite: ['+91', 'IN'],
                      // optional. Shows only country name and flag
                      showCountryOnly: false,
                      // optional. Shows only country name and flag when popup is closed.
                      showOnlyCountryWhenClosed: false,
                      showFlag: false,
                      // optional. aligns the flag and the Text left
                      alignLeft: false,
                      textStyle: TextStyle(
                          fontFamily: FontFamily.Gilroy,
                          fontSize: FontSize.FONT_13,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  hintText: AppStrings.MOBILE,
                  hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                  fillColor: AppColors.APP_COLOR_WHITE,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.APP_COLOR_PRIMARY,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.halfWhite,
                      width: 2.0,
                    ),
                  ),
                ),
                validator: (val) {
                  if (val.length == 0) {
                    return AppStrings.MOBILE_CANNOT_BE_EMPTY;
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.phone,
                style: new TextStyle(
                  fontSize: FontSize.FONT_15,
                  letterSpacing: 2.0,
                  fontFamily: FontFamily.Poppins,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: RaisedButton(
                  padding: EdgeInsets.all(10),
                  onPressed: () {
                    getStringValuesSF();
                    widget.phoneNumberSignInFunction();
                    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => OtpScreen()));
                  },
                  color: widget.buttonColor != null
                      ? widget.buttonColor
                      : AppColors.APP_COLOR_PRIMARY,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    side: BorderSide(color: AppColors.APP_COLOR_PRIMARY),
                  ),
                  child: Text(
                    AppStrings.NEXT,
                    style: TextStyle(
                        fontFamily: FontFamily.Gilroy,
                        fontSize: FontSize.FONT_20,
                        color: AppColors.APP_COLOR_WHITE,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget get _emailWidget {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5.0),
            topRight: Radius.circular(5.0),
          ),
        ),
        child: Form(
          key: widget.globalKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: Text(
                  AppStrings.SIGN_UP,
                  style: TextStyle(
                    fontFamily: FontFamily.Gilroy,
                    fontSize: FontSize.FONT_20,
                    color: AppColors.APP_COLOR_PRIMARY,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.42,
                    child: TextFormField(
                      controller: widget.firstNameController,
                      decoration: new InputDecoration(
                        hintText: AppStrings.FIRST_NAME,
                        hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                        fillColor: AppColors.APP_COLOR_WHITE,

                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7.0),
                          borderSide: BorderSide(
                            color: AppColors.APP_COLOR_PRIMARY,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7.0),
                          borderSide: BorderSide(
                            color: AppColors.halfWhite,
                            width: 2.0,
                          ),
                        ),

                        //fillColor: Colors.green
                      ),
                      validator: (val) {
                        if (val.length == 0) {
                          return AppStrings.FIRST_NAME_CANNOT_BE_EMPTY;
                        } else {
                          return null;
                        }
                      },
                      keyboardType: TextInputType.name,
                      style: new TextStyle(
                        fontFamily: FontFamily.Poppins,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.42,
                    child: TextFormField(
                      controller: widget.lastNameController,
                      decoration: new InputDecoration(
                        hintText: AppStrings.LAST_NAME,
                        hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                        fillColor: AppColors.APP_COLOR_WHITE,

                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7.0),
                          borderSide: BorderSide(
                            color: AppColors.APP_COLOR_PRIMARY,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(7.0),
                          borderSide: BorderSide(
                            color: AppColors.halfWhite,
                            width: 2.0,
                          ),
                        ),

                        //fillColor: Colors.green
                      ),
                      validator: (val) {
                        if (val.length == 0) {
                          return AppStrings.LAST_NAME_CANNOT_BE_EMPTY;
                        } else {
                          return null;
                        }
                      },
                      keyboardType: TextInputType.name,
                      style: new TextStyle(
                        fontFamily: FontFamily.Poppins,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: widget.emailController,
                decoration: new InputDecoration(
                  hintText: AppStrings.EMAIL,
                  hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                  fillColor: AppColors.APP_COLOR_WHITE,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.APP_COLOR_PRIMARY,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.halfWhite,
                      width: 2.0,
                    ),
                  ),
                ),
                validator: (val) {
                  if (!SystemUtils.hasMatchEmail(val)) {
                    return AppStrings.INVALID_EMAIL;
                  }
                  if (val.length == 0) {
                    return AppStrings.EMAIL_CANNOT_BE_EMPTY;
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.emailAddress,
                style: new TextStyle(
                  fontFamily: FontFamily.Poppins,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: widget.passwordController,
                obscureText: !_obscureText,
                decoration: new InputDecoration(
                  suffixIcon: GestureDetector(
                    onTap: () {
                      _toggle();
                    },
                    child: Icon(
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off,
                      color: !_passwordVisible
                          ? AppColors.halfWhite
                          : AppColors.textColor,
                    ),
                  ),
                  hintText: AppStrings.PASSWORD,
                  hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                  fillColor: AppColors.APP_COLOR_WHITE,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.APP_COLOR_PRIMARY,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.halfWhite,
                      width: 2.0,
                    ),
                  ),
                ),
                validator: (val) {
                  if (val.length == 0) {
                    return AppStrings.PASSWORD_CANNOT_BE_EMPTY;
                  } else if (val.length < 6) {
                    return AppStrings.PASSWORD_TOO_SHORT;
                  } else {
                    return null;
                  }
                },
                keyboardType: TextInputType.text,
                style: new TextStyle(
                  fontFamily: FontFamily.Gilroy,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: RaisedButton(
                  padding: EdgeInsets.all(10),
                  onPressed: () {
                    setState(() {
                      getStringValuesSF();
                      widget.emailPasswordSignInFunction();
                    });
                  },
                  color: AppColors.APP_COLOR_PRIMARY,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    side: BorderSide(color: AppColors.APP_COLOR_PRIMARY),
                  ),
                  child: Text(
                    AppStrings.SIGN_UP,
                    style: TextStyle(
                        fontFamily: FontFamily.Gilroy,
                        fontSize: FontSize.FONT_20,
                        color: AppColors.APP_COLOR_WHITE,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      AppStrings.ALREADY_HAVE_AN_ACCOUNT,
                      style: TextStyle(
                          fontFamily: FontFamily.Gilroy,
                          fontSize: FontSize.FONT_13,
                          color: AppColors.APP_COLOR_PRIMARY,
                          fontWeight: FontWeight.normal),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      widget.signInFunction();
                    },
                    child: Container(
                      child: Text(
                        AppStrings.SIGN_IN,
                        style: TextStyle(
                            fontFamily: FontFamily.Gilroy,
                            fontSize: 15,
                            color: AppColors.APP_COLOR_PRIMARY,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onCountryChange(CountryCode countryCode) {
    this.phoneNumber = countryCode.toString();
  }
}
