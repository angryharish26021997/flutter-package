import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:harish_package/Constants/AppColors.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'Constants/SvgPictures.dart';
import 'Constants/ImageAssets.dart';

// ignore: must_be_immutable
class HomeScreenComponent extends StatefulWidget {
  final Function() onMenuPressed;
  final Color backgroundColor;

  HomeScreenComponent({
    Key key,
    @required this.onMenuPressed,
    @required this.backgroundColor,
  });

  @override
  _HomeScreenComponentState createState() => _HomeScreenComponentState();
}

class _HomeScreenComponentState extends State<HomeScreenComponent> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.backgroundColor!=null ? widget.backgroundColor : AppColors.APP_COLOR_WHITE,
      body: SafeArea(
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                height: 60,
                color: AppColors.APP_COLOR_PRIMARY,
                child: Row(
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(32.0)),
                      child: Material(
                        shadowColor: Colors.transparent,
                        color: AppColors.APP_COLOR_PRIMARY,
                        child: InkWell(
                          splashColor: AppColors.errorWarning,
                          focusColor:  AppColors.APP_COLOR_WHITE,
                          onTap: () {
                            widget.onMenuPressed();
                          },
                          child:  Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Icon(
                              Icons.menu,
                              color: Colors.white,
                            ),
                          ),
                        ),

                        /* IconButton(
                          icon: Icon(
                            Icons.menu,
                            color: Colors.black,
                          ),
                          onPressed: widget.onMenuPressed,
                        ),*/
                      ),
                    ),
                    SizedBox(width: 130,),
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(32.0)),
                      child: Material(
                        shadowColor: AppColors.APP_COLOR_PRIMARY,
                        color: AppColors.APP_COLOR_PRIMARY,
                        child: InkWell(
                          splashColor: AppColors.errorWarning,
                          focusColor:  AppColors.APP_COLOR_WHITE,
                          onTap: () {
                            //widget.onMenuPressed();
                          },
                          child:  Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Icon(
                              Icons.notifications_active,
                              color: Colors.white,
                            ),
                          ),
                        ),

                        /* IconButton(
                            icon: Icon(
                              Icons.menu,
                              color: Colors.black,
                            ),
                            onPressed: widget.onMenuPressed,
                          ),*/
                      ),
                    ),
                    SizedBox(width: 10,),
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(32.0)),
                      child: Material(
                        shadowColor: AppColors.APP_COLOR_PRIMARY,
                        color: AppColors.APP_COLOR_PRIMARY,
                        child: InkWell(
                          splashColor: AppColors.errorWarning,
                          focusColor:  AppColors.APP_COLOR_WHITE,
                          onTap: () {
                            //widget.onMenuPressed();
                          },
                          child:  Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Icon(
                              Icons.settings,
                              color: Colors.white,
                            ),
                          ),
                        ),

                        /* IconButton(
                            icon: Icon(
                              Icons.menu,
                              color: Colors.black,
                            ),
                            onPressed: widget.onMenuPressed,
                          ),*/
                      ),
                    ),
                    SizedBox(width: 10,),
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(32.0)),
                      child: Material(
                        shadowColor: AppColors.APP_COLOR_PRIMARY,
                        color: AppColors.APP_COLOR_PRIMARY,
                        child: InkWell(
                          splashColor: AppColors.errorWarning,
                          focusColor:  AppColors.APP_COLOR_WHITE,
                          onTap: () {
                            //widget.onMenuPressed();
                          },
                          child:  Padding(
                            padding: const EdgeInsets.all(12.0),
                            child: Icon(
                              Icons.messenger_rounded,
                              color: Colors.white,
                            ),
                          ),
                        ),

                        /* IconButton(
                            icon: Icon(
                              Icons.menu,
                              color: Colors.black,
                            ),
                            onPressed: widget.onMenuPressed,
                          ),*/
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('Home Screen'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
