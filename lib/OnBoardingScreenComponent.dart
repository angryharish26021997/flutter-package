import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:scrolling_page_indicator/scrolling_page_indicator.dart';

import 'Constants/AppColors.dart';
import 'Constants/AppStrings.dart';
import 'Constants/fontFamily.dart';
import 'Constants/fontSize.dart';



class OnBoardingScreenComponent extends StatefulWidget {
  final List<Widget> items;
  final Function() signInMethod;
  final Function() skipMethod;

  OnBoardingScreenComponent(
      {Key key,
      @required this.items,
      @required this.signInMethod,
      @required this.skipMethod})
      : super(key: key);

  @override
  _OnBoardingScreenComponentState createState() =>
      _OnBoardingScreenComponentState();
}

class _OnBoardingScreenComponentState extends State<OnBoardingScreenComponent> {


  PageController _controller = PageController(initialPage: 0);

  int currentPage = 0;
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  BuildContext buildContext;
  @override
  void initState() {
    _controller = PageController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    buildContext = context;
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.1,
            ),
            Expanded(
              child: PageView(
                onPageChanged: (index) {
                  setState(() {
                    currentPage = index;
                  });

                },
                children: widget.items,
                controller: _controller,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    widget.skipMethod();
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                    AppStrings.SKIP,
                      style: TextStyle(
                        fontFamily: FontFamily.Poppins,
                        fontSize: FontSize.FONT_20,
                        color: AppColors.APP_GRAY_1,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                ),
                ScrollingPageIndicator(
                  dotColor: AppColors.dotColor,
                  dotSelectedColor: AppColors.APP_COLOR_PRIMARY,
                  dotSize: FontSize.DOT_06,
                  dotSelectedSize: FontSize.DOT_08,
                  dotSpacing: FontSize.DOT_20,
                  controller: _controller,
                  itemCount: widget.items.length,
                  orientation: Axis.horizontal,
                ),
                InkWell(
                  onTap: () {
                    if (currentPage != widget.items.length - 1) {
                      _controller.nextPage(
                          duration: _kDuration, curve: _kCurve);
                    } else {
                      widget.signInMethod();
                    }
                  },
                  child: Container(
                    padding: EdgeInsets.all(10),
                    child: currentPage != widget.items.length - 1
                        ? Text(
                            AppStrings.NEXT,
                            style: TextStyle(
                              fontFamily: FontFamily.Poppins,
                              fontSize: FontSize.FONT_20,
                              color: AppColors.APP_COLOR_PRIMARY,
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.right,
                          )
                        : Text(
                           AppStrings.SIGN_UP_SMALL,
                            style: TextStyle(
                              fontFamily: FontFamily.Poppins,
                              fontSize: FontSize.FONT_20,
                              color: AppColors.APP_COLOR_PRIMARY,
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.right,
                          ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}

Widget buildPage(String title, Color color, String svgImage, String description) {
  BuildContext buildContext;

  return Column(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: [
      Container(
          child: Text(
        title,
        style: TextStyle(
          fontFamily: FontFamily.Poppins,
          fontSize: 25,
          color: AppColors.APP_COLOR_PRIMARY,
          fontWeight: FontWeight.w700,
        ),
      )),
      SizedBox(
        height: 40,
      ),
    /*  Container(
        child: SvgPicture.network(
          test,
          placeholderBuilder: (context) => CircularProgressIndicator(
            backgroundColor: AppColors.APP_COLOR_PRIMARY,
          ),
          height: 128.0,
        ),
      ),*/
        Container(
          height: 100,
        child:SvgPicture.asset(
          svgImage,
          placeholderBuilder: (context) => CircularProgressIndicator(),
          height: 128.0,
          allowDrawingOutsideViewBox: true,
        ),
      ),


      SizedBox(
        height: 40,
      ),
      Expanded(
        child: Container(
          child: Text(
            description,
            style: TextStyle(
              fontFamily: FontFamily.Poppins,
              fontSize: FontSize.FONT_13,
              color: AppColors.APP_GRAY_1,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ),
    ],
  );
}
