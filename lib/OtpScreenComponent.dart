import 'package:code_fields/code_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:harish_package/Utils/SystemUtils.dart';

import 'Constants/AppColors.dart';
import 'Constants/AppStrings.dart';
import 'Constants/SvgPictures.dart';
import 'Constants/fontFamily.dart';

class OtpScreenComponent extends StatefulWidget {
  final Color backgroundColor;
  final Color buttonColor;
  final String title;
  final GlobalKey globalKey;
  final Function() otpSuccess;
  final Function(String val) validateCondition;

  OtpScreenComponent({
    Key key,
    @required this.backgroundColor,
    @required this.buttonColor,
    @required this.title,
    @required this.globalKey,
    @required this.otpSuccess,
    @required this.validateCondition,
  }) : super(key: key);

  @override
  _OtpScreenComponentState createState() => _OtpScreenComponentState();
}

class _OtpScreenComponentState extends State<OtpScreenComponent> {
  bool isValidOTPErrorMessageShowingText = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.backgroundColor != null
          ? widget.backgroundColor
          : AppColors.APP_COLOR_PRIMARY,
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                top: 60.0, left: 30.0, right: 30.0, bottom: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(
                    AppStrings.LOGO,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 50.0,
                        fontFamily: FontFamily.Gilroy),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          _mobilWidget,
        ],
      ),
    );
  }

  Widget get _mobilWidget {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5.0),
            topRight: Radius.circular(5.0),
          ),
        ),
        child: Form(
          key: widget.globalKey,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: Text(
                  AppStrings.VERIFICATION,
                  style: TextStyle(
                    fontFamily: FontFamily.Gilroy,
                    fontSize: 20,
                    color: AppColors.APP_COLOR_PRIMARY,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              /*TextFormField(
                onChanged: (val) {
                  if (val.length == 6) {
                    SystemUtils.hideKeyboard(context);
                  }
                },
                decoration: new InputDecoration(
                  hintText: AppStrings.OTP,
                  hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                  fillColor: AppColors.APP_COLOR_WHITE,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.APP_COLOR_PRIMARY,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.halfWhite,
                      width: 2.0,
                    ),
                  ),
                ),
                validator: (val) {
                  if (val.length == 0) {
                    setState(() {
                      isValidOTPErrorMessageShowingText = true;
                    });
                    return AppStrings.INVALID_OTP;
                  } else {
                    setState(() {
                      isValidOTPErrorMessageShowingText = false;
                    });
                    return null;
                  }
                },
                keyboardType: TextInputType.phone,
                style: new TextStyle(
                  fontFamily: FontFamily.Poppins,
                ),
              ),*/
              CodeFields(
                closeOnFinish: true,
                fieldWidth: 47,
                fieldHeight: 70,
                keyboardType: TextInputType.number,
                length: 5,
                autofocus: true,
                validator: (val) {
                  widget.validateCondition(val);
                  if (val.length == 0) {
                    return AppStrings.OTP_CANNOT_BE_EMPTY;
                  } else if (val.length != 5) {
                    return AppStrings.PLEASE_COMPLETE_THE_OTP;
                  } else {
                    return null;
                  }
                },
                inputDecoration: InputDecoration(

                  filled: true,
                  fillColor: Colors.grey.withOpacity(0.1),
                  enabledBorder: OutlineInputBorder(

                      borderRadius: BorderRadius.circular(12.0),
                      borderSide: BorderSide(color: AppColors.APP_COLOR_PRIMARY)),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.4,
                child: RaisedButton(
                  padding: EdgeInsets.all(10),
                  onPressed: () {
                    widget.otpSuccess();
                    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => OtpScreen()));
                  },
                  color: AppColors.APP_COLOR_PRIMARY,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    side: BorderSide(color: AppColors.APP_COLOR_PRIMARY),
                  ),
                  child: Text(
                    AppStrings.CONFIRM,
                    style: TextStyle(
                        fontFamily: FontFamily.Gilroy,
                        fontSize: 20,
                        color: AppColors.APP_COLOR_WHITE,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }

  String validateCode(String value) {
    print("Code Validate");
  }
}
