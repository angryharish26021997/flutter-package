import 'package:flutter/material.dart';

class StepperComponent extends StatefulWidget {
  final List<Step> steps;
  final String title = "Stepper Demo";
  Function() callBack;

  StepperComponent({Key key, this.steps, this.callBack}) : super(key: key);

  @override
  StepperComponentState createState() => StepperComponentState();
}

class StepperComponentState extends State<StepperComponent> {
  //
  int current_step = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Appbar
      appBar: AppBar(
        // Title
        title: Text("Stepper Component"),
      ),
      // Body
      body: Container(
        child: Stepper(
          key: Key("mysuperkey-" +  widget.steps.length.toString()),
          currentStep: this.current_step,
          steps: widget.steps,
          type: StepperType.horizontal,
          onStepTapped: (step) {
            setState(() {
              current_step = step;
            });
          },
          onStepContinue: () {
            setState(() {
              this.mounted;

              // widget.callBack();
              if (current_step < widget.steps.length - 1) {
                current_step = current_step + 1;

                print("steps====" + current_step.toString());
              }
            });
          },
          onStepCancel: () {
            this.mounted;
            setState(() {
              if (current_step > 0) {
                print("steps3");
                current_step = current_step - 1;
              }
            });
          },
        ),
      ),
    );
  }
}
