import 'package:flutter/material.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'Constants/AppColors.dart';
import 'Constants/AppStrings.dart';
import 'Constants/fontFamily.dart';
import 'package:harish_package/Constants/fontSize.dart';
import 'package:harish_package/Utils/SystemUtils.dart';

class LoginScreenComponent extends StatefulWidget {
  final Color backgroundPrimaryColor;
  final Color buttonColor;
  final String title;
  final Function() signInFunction;
  final Function() signUpFunction;
  final Function() forgotFunction;
  final TextEditingController passwordController;
  final TextEditingController emailController;
  final GlobalKey globalKey;

  LoginScreenComponent({
    Key key,
    @required this.backgroundPrimaryColor,
    @required this.buttonColor,
    @required this.title,
    @required this.signInFunction,
    @required this.signUpFunction,
    @required this.forgotFunction,
    @required this.passwordController,
    @required this.emailController,
    @required this.globalKey,
  }) : super(key: key);

  @override
  _LoginScreenComponentState createState() => _LoginScreenComponentState();
}

class _LoginScreenComponentState extends State<LoginScreenComponent> {
  bool _obscureText = false;
  bool _passwordVisible = false;

  String _password;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
      _passwordVisible = !_passwordVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.backgroundPrimaryColor != null
          ? widget.backgroundPrimaryColor
          : AppColors.APP_COLOR_PRIMARY,
      body: ListView(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(
                top: 60.0, left: 30.0, right: 30.0, bottom: 30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Center(
                  child: Text(
                    widget.title != null ? widget.title : AppStrings.LOGO,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: FontSize.FONT_50,
                        fontFamily: FontFamily.Gilroy),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
          _emailWidget,
        ],
      ),
    );
  }

  Widget get _emailWidget {
    return Container(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        padding: EdgeInsets.symmetric(horizontal: 20.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(5.0),
            topRight: Radius.circular(5.0),
          ),
        ),
        child: Form(
          key: widget.globalKey,
          child: Column(
            children: [
              Container(
                alignment: Alignment.center,
                padding: EdgeInsets.all(20),
                child: Text(
                  AppStrings.SIGN_IN,
                  style: TextStyle(
                    fontFamily: FontFamily.Gilroy,
                    fontSize: 20,
                    color: AppColors.APP_COLOR_PRIMARY,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),

              TextFormField(
                controller: widget.emailController,
                decoration: new InputDecoration(
                  hintText: AppStrings.EMAIL,
                  hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                  fillColor: AppColors.APP_COLOR_WHITE,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.APP_COLOR_PRIMARY,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.halfWhite,
                      width: 2.0,
                    ),
                  ),
                ),
                validator: (val) {

                  if (val.length == 0) {
                    return AppStrings.EMAIL_CANNOT_BE_EMPTY;
                  }
                  else if (!SystemUtils.hasMatchEmail(val)) {
                    return AppStrings.INVALID_EMAIL;
                  }
                  else {
                    return null;
                  }
                },
                keyboardType: TextInputType.emailAddress,
                style: new TextStyle(
                  fontFamily: FontFamily.Poppins,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: widget.passwordController,
                obscureText: !_obscureText,
                decoration: new InputDecoration(
                  suffixIcon: GestureDetector(
                    onTap: () {
                      _toggle();
                    },
                    child: Icon(
                      _passwordVisible ? Icons.visibility : Icons.visibility_off,
                      color: !_passwordVisible
                          ? AppColors.halfWhite
                          : AppColors.textColor,
                    ),
                  ),
                  hintText: AppStrings.PASSWORD,
                  hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
                  fillColor: AppColors.APP_COLOR_WHITE,
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.APP_COLOR_PRIMARY,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    borderSide: BorderSide(
                      color: AppColors.halfWhite,
                      width: 2.0,
                    ),
                  ),
                ),
                validator: (val) {
                  if (val.length == 0) {
                    return AppStrings.PASSWORD_CANNOT_BE_EMPTY;
                  }

                  else if(val.length<6){
                    return AppStrings.PASSWORD_TOO_SHORT;
                  }
                  else {
                    return null;
                  }
                },
                keyboardType: TextInputType.text,
                style: new TextStyle(
                  fontFamily: FontFamily.Gilroy,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                padding: EdgeInsets.only(left: 8),
                alignment: Alignment.centerLeft,
                child: InkWell(
                  onTap: () {
                    widget.forgotFunction();
                  },
                  child: Text(
                    AppStrings.FORGOT_PASSWORD,
                    style: TextStyle(
                        fontFamily: FontFamily.Gilroy,
                        fontSize: 15,
                        color: AppColors.APP_COLOR_PRIMARY,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width * 0.4,
                child: RaisedButton(
                  padding: EdgeInsets.all(10),
                  onPressed: () {
                    widget.signInFunction();
                  },
                  color: AppColors.APP_COLOR_PRIMARY,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7.0),
                    side: BorderSide(color: AppColors.APP_COLOR_PRIMARY),
                  ),
                  child: Text(
                    AppStrings.SIGN_IN,
                    style: TextStyle(
                        fontFamily: FontFamily.Gilroy,
                        fontSize: FontSize.FONT_20,
                        color: AppColors.APP_COLOR_WHITE,
                        fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Text(
                      AppStrings.CREATE_AN_ACCOUNT,
                      style: TextStyle(
                          fontFamily: FontFamily.Gilroy,
                          fontSize: FontSize.FONT_13,
                          color: AppColors.APP_COLOR_PRIMARY,
                          fontWeight: FontWeight.normal),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      widget.signUpFunction();
                    },
                    child: Container(
                      child: Text(
                        AppStrings.SIGN_UP,
                        style: TextStyle(
                            fontFamily: FontFamily.Gilroy,
                            fontSize: 15,
                            color: AppColors.APP_COLOR_PRIMARY,
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
