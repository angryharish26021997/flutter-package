import 'package:flutter/material.dart';
import '../Constants/AppStrings.dart';
import '../Constants/AppColors.dart';
import '../Constants/FontFamily.dart';
import '../Constants/FontSize.dart';

class SystemUtils {
  List<dynamic> jsonArray;

  static void hideKeyboard(BuildContext context) =>
      FocusScope.of(context).requestFocus(new FocusNode());

  static hasMatchEmail(String mail) => RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(mail);

  static firstName(String name) => Container(
        child: Text(name),
      );

  static colorRed(String red) => Container(
        color: Colors.red,
        height: 100,
        width: 100,
      );

  static colorBlue(String blue) => Container(
        color: Colors.blue,
        height: 100,
        width: 100,
      );

  static textFromFieldComponent(
          BuildContext context,
          String labelName,
          TextEditingController textEditingController,
          String validator,
          String validatorErrorMessage,
          String inputType) =>
      Container(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(10),
          child: TextFormField(
            controller: textEditingController,
            decoration: new InputDecoration(
              hintText: labelName ?? "test",
              hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
              fillColor: AppColors.APP_COLOR_WHITE,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(7.0),
                borderSide: BorderSide(
                  color: AppColors.APP_COLOR_PRIMARY,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(7.0),
                borderSide: BorderSide(
                  color: AppColors.halfWhite,
                  width: 2.0,
                ),
              ),

              //fillColor: Colors.green
            ),
            validator: (val) {
              Pattern pattern = validator;
              RegExp regex = new RegExp(pattern);
              if (!regex.hasMatch(val))
                return validatorErrorMessage;
              else
                return null;
              /*if (val.length == 0) {
                return AppStrings.FIRST_NAME_CANNOT_BE_EMPTY;
              }
              else if(val.contains(validator)){
                return "Test";
              }
              else {
                return null;
              }*/
            },
            keyboardType: inputType == "Email"
                ? TextInputType.emailAddress
                : TextInputType.text,
            style: new TextStyle(
              fontFamily: FontFamily.Poppins,
            ),
          ),
        ),
      );

  /*static textAreaComponent(BuildContext context, String labelName,
       String validator,String validatorErrorMessage) =>*/
  static textAreaComponent(
    BuildContext context,
      TextEditingController textEditingController,
      String labelText
  ) =>
      Container(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(10),
          child: TextFormField(
            controller: textEditingController,
            decoration: new InputDecoration(
              contentPadding: EdgeInsets.only(left: 8, top: 100),
              hintText: labelText,
              hintStyle: TextStyle(fontFamily: FontFamily.Gilroy),
              fillColor: AppColors.APP_COLOR_WHITE,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(7.0),
                borderSide: BorderSide(
                  color: AppColors.APP_COLOR_PRIMARY,
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(7.0),
                borderSide: BorderSide(
                  color: AppColors.halfWhite,
                  width: 2.0,
                ),
              ),

              //fillColor: Colors.green
            ),
            validator: (val) {
              /* Pattern pattern = validator;
                      RegExp regex = new RegExp(pattern);
                      if (!regex.hasMatch(val))
                        return validatorErrorMessage;
                      else
                        return null;*/
              if (val.length == 0) {
                return AppStrings.REQUIRED_THIS_FILED;
              } else {
                return null;
              }
            },
            keyboardType: TextInputType.text,
            style: new TextStyle(
              fontFamily: FontFamily.Poppins,
            ),
            maxLength: 1000,
          ),
        ),
      );

  static dropDownComponent({Function() onChanged}) => DropdownButton<String>(
        value: "dropDownValue",
        icon: Icon(Icons.arrow_downward),
        iconSize: 24,
        elevation: 16,
        style: TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (String newValue) {
          onChanged();
        },
        items: <String>['One', 'Two', 'Free', 'Four']
            .map<DropdownMenuItem<String>>((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Text("" + value),
          );
        }).toList(),
      );
}
