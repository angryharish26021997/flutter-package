class FontSize{
  static final double FONT_12 = 12;
  static final double FONT_13 = 13;
  static final double FONT_15 = 15;
  static final double FONT_20 = 20;
  static final double FONT_30 = 30;
  static final double FONT_50 = 50;


  // dot Size
  static final double DOT_06 = 06;
  static final double DOT_08 = 08;
  static final double DOT_20 = 20;
}