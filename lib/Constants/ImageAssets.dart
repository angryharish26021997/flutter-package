class ImageAssets{
  static final String FACEBOOK = "assets/images/facebook.png";
  static final String GMAIL = "assets/images/google.png";
  static final String HIDDEN = "assets/images/hidden.png";
  static final String VISIBLE = "assets/images/visible.png";
  static final String MENU = "assets/images/menu.png";
}
