import 'dart:ui';
import 'package:flutter/material.dart';

class AppColors{
  static const Color APP_COLOR_PRIMARY = Color(0xff0F1D40);
  static const Color APP_COLOR_WHITE = Color(0xffffffff);
  static const Color APP_COLOR_BLACK = Color(0xff000000);
  static const Color dotColor = Color(0xff707070);
  static const Color halfWhite = Color(0xffDCDCDC);
  static const Color errorWarning = Color(0xffff4a06);
  static const Color textColor = Color(0xffA9A9A9);
  static const Color FACEBOOK_COLOR = Color(0xff1877F2);
  static const Color APP_GRAY_1 = Color(0xffadadad);



  static const int _whitePrimaryValue = 0xFFffffff;
  static MaterialColor MATERIAL_WHITE = MaterialColor(
    _whitePrimaryValue,
    <int, Color>{
      70: Color(0xB3FFFFFF),
      60: Color(0x99FFFFFF),
      54: Color(0x8AFFFFFF),
      38: Color(0x62FFFFFF),
      30: Color(0x4DFFFFFF),
      50: Color(_whitePrimaryValue),
      24: Color(0x3DFFFFFF),
      12: Color(0x1FFFFFFF),
      10: Color(0x1AFFFFFF),
    },
  );
}