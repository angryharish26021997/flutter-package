import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';
import './DemoComponent.dart';
import 'Constants/ImageAssets.dart';
import 'Constants/AppColors.dart';
import 'Constants/fontFamily.dart';

class DashBoardComponent extends StatefulWidget {
  final KFDrawerController drawerController;
  final Function() signOutFunction;
  final Function(int index) bottomBarFunction;

  DashBoardComponent({
    Key key,
    @required this.drawerController,
    @required this.signOutFunction,
    @required this.bottomBarFunction,
  }) : super(key: key);

  @override
  _DashBoardComponentState createState() => _DashBoardComponentState();
}

class _DashBoardComponentState extends State<DashBoardComponent>
    with TickerProviderStateMixin {
  //KFDrawerController _drawerController;

  @override
  void initState() {
    super.initState();
  }

  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });

    if (_selectedIndex == 0) {
      widget.bottomBarFunction(_selectedIndex);
    }

    else if (_selectedIndex == 1) {
      widget.bottomBarFunction(_selectedIndex);
    }

    else if (_selectedIndex == 2) {
      widget.bottomBarFunction(_selectedIndex);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.APP_COLOR_PRIMARY,
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home,),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.business),
            label: 'Demo',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor:AppColors.APP_COLOR_PRIMARY,
        onTap: _onItemTapped,
      ),
      body: KFDrawer(
        controller: widget.drawerController,
        header: Align(
          alignment: Alignment.centerLeft,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            width: MediaQuery.of(context).size.width * 0.6,
            child: Column(
              children: [
                Image.asset(
                  ImageAssets.GMAIL,
                  alignment: Alignment.centerLeft,
                ),
              ],
            ),
          ),
        ),
        footer: KFDrawerItem(
          text: Text(
            'SIGN IN',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(
            Icons.input,
            color: Colors.white,
          ),
          onPressed: () {
            widget.signOutFunction();
          },
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromRGBO(255, 255, 255, 1.0),
              AppColors.APP_COLOR_PRIMARY
            ],
            tileMode: TileMode.repeated,
          ),
        ),
      ),
    );
  }
}
