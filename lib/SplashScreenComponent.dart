library harish_package;

import 'package:flutter/material.dart';
import 'package:harish_package/Constants/AppColors.dart';
import 'package:harish_package/Constants/fontFamily.dart';

class SplashScreenComponent extends StatefulWidget {
  final String image;
  final Color color;
  final String text;
  final Color backgroundColor = Colors.white;
  final Function() function;

  final TextStyle styleTextUnderTheLoader = TextStyle(
      fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.black);

  SplashScreenComponent(
      {Key key,
      @required this.image,
      @required this.color,
      @required this.text,
      @required this.function})
      : super(key: key);

  @override
  _SplashScreenComponentState createState() => _SplashScreenComponentState();
}

class _SplashScreenComponentState extends State<SplashScreenComponent> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor:
          widget.color != null ? widget.color : AppColors.APP_COLOR_PRIMARY,
      body: InkWell(
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Visibility(
                  visible: true,
                  child: Expanded(
                    flex: 7,
                    child: Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        widget.image != null
                            ? Image.asset(
                                widget.image,
                                height: 300,
                                width: 300,
                                color: Colors.red,
                              )
                            : Icon(
                                Icons.android,
                                color: Colors.red,
                                size: 100,
                              ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10.0),
                        ),
                      ],
                    )),
                  ),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      CircularProgressIndicator(
                        backgroundColor: Colors.transparent,
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.red),
                      ),
                      Container(
                        height: 10,
                      ),
                      Center(
                          child: Text(
                              widget.text != null ? widget.text : "Android",
                              style: TextStyle(

                                  fontFamily: FontFamily.Gilroy,
                                  color: AppColors.APP_COLOR_WHITE))),
                    ],
                  ),
                ),
                RaisedButton(
                  child: Text("Move next screen"),
                  color: Colors.red,
                  onPressed: () {
                    widget.function();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
