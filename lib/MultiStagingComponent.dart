import 'package:flutter/material.dart';
import 'package:im_stepper/stepper.dart';
import 'Utils/SystemUtils.dart';
import 'Constants/AppStrings.dart';
import 'Constants/AppColors.dart';
import 'Constants/fontFamily.dart';
import 'Constants/fontSize.dart';
import 'Constants/SvgPictures.dart';
import 'Constants/ImageAssets.dart';
import 'package:toast/toast.dart';

// ignore: must_be_immutable
class MultiStagingComponent extends StatefulWidget {
  final String firstName;
  final String lastName;
  final String colorName;
  final Color backGroundColorName;
  var jsonData;
  List<dynamic> jsonArray;
  List<Widget> items;
  final GlobalKey formKey;
  List<int> numberStepperCount;

  Function() onPressed;

  List<TextEditingController> controllers;
  List<TextEditingController> textAreaController;

  List<Map> myJson;

  var dynamicJsonField;

  MultiStagingComponent({
    Key key,
    this.firstName,
    this.lastName,
    this.colorName,
    this.jsonData,
    this.jsonArray,
    this.backGroundColorName,
    this.numberStepperCount,
    this.items,
    this.controllers,
    this.textAreaController,
    this.formKey,
    this.onPressed,
    this.myJson,
    this.dynamicJsonField,
  }) : super(key: key);

  @override
  _MultiStagingComponentState createState() => _MultiStagingComponentState();
}

class _MultiStagingComponentState extends State<MultiStagingComponent> {
  List<TextEditingController> controllers = [];
  int countPage = 1;
  String dropdownValue = "One";
  PageController _controller = PageController(initialPage: 0);
  int currentPage = 0;
  static const _kDuration = const Duration(milliseconds: 300);
  static const _kCurve = Curves.ease;

  BuildContext buildContext;

  String _mySelection;

  List<Map> _data;

  int _currVal = 1;
  String _currText = '';

  List<GroupModel> _group = [
    GroupModel(
      text: "Flutter.dev",
      index: 1,
    ),
    GroupModel(
      text: "Inducesmile.com",
      index: 2,
    ),
    GroupModel(
      text: "Google.com",
      index: 3,
    ),
    GroupModel(
      text: "Yahoo.com",
      index: 4,
    ),
  ];

  @override
  void initState() {
    _controller = PageController();
    // TODO: implement initState
    super.initState();
    controllers =
        List.generate(widget.jsonArray.length, (i) => TextEditingController());

    for (int i = 0; i < widget.jsonArray.length; i++) {
      if (widget.jsonArray[i]['fieldValue'].toString() == "DropDown") {
        _data = widget.jsonArray[i]['dropDownList'];

        //_data.add(widget.jsonArray[i]['dropDownList']);
        print("Data" + _data.toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: widget.backGroundColorName ?? AppColors.APP_COLOR_WHITE,
      body: Center(
        child: Form(
          key: widget.formKey,
          child: ListView(
            /* shrinkWrap: true,*/
            children: [
              /*widget.numberStepperCount != null
                  ? Container(
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: NumberStepper(
                        enableNextPreviousButtons: true,
                        scrollingDisabled: false,
                        enableStepTapping: true,
                        activeStepColor: Colors.green,
                        numbers: widget.numberStepperCount,
                        lineColor: Colors.purple,
                      ),
                    )
                  : null,*/
              for (int i = 0; i < widget.jsonArray.length; i++)
                if (widget.jsonArray[i]['fieldValue'].toString() == "DropDown")
                  Card(
                    elevation: 10.0,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      height: 40,
                      child: DropdownButton<String>(
                        underline: Container(),
                        isDense: false,
                        isExpanded: true,
                        hint: new Text("Select"),
                        value: _mySelection,
                        onChanged: (String newValue) {
                          setState(() {
                            _mySelection = newValue;
                          });

                          print(_mySelection);
                        },
                        items: _data.map((Map map) {
                          for (int i = 0; i < _data.length; i++) {
                            return new DropdownMenuItem<String>(
                              value: map["id"].toString(),
                              child: new Text(
                                map["name"],
                              ),
                            );
                          }
                        }).toList(),
                      ),
                    ),
                  ),
              for (int i = 0; i < widget.dynamicJsonField.length; i++)
                if (widget.dynamicJsonField[i]['fieldValue'].toString() ==
                    "text")
                  SystemUtils.textFromFieldComponent(
                      context,
                      widget.dynamicJsonField[i]['labelText'].toString(),
                      widget.controllers[i],
                      widget.dynamicJsonField[i]['validator'].toString(),
                      widget.dynamicJsonField[i]['validatorErrorMessage']
                          .toString(),
                      widget.dynamicJsonField[i]['inputType'].toString()),
              for (int i = 0; i < widget.dynamicJsonField.length; i++)
                if (widget.dynamicJsonField[i]['fieldValue'].toString() ==
                    "textArea")
                  SystemUtils.textAreaComponent(
                      context,
                      widget.textAreaController[i],
                      widget.dynamicJsonField[i]['labelText'].toString()),
              for (int i = 0; i < widget.dynamicJsonField.length; i++)
                if (widget.dynamicJsonField[i]['id'] == "checkBox1")
                  CheckboxListTile(
                    title: Text(widget.dynamicJsonField[i]['name']),
                    value: widget.dynamicJsonField[i]['is_active'],
                    onChanged: (val) {
                      setState(() {
                        widget.dynamicJsonField[i]['is_active'] = val;
                      });
                    },
                  ),
              Container(
                child: Center(
                  child: Text(_currText,
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      )),
                ),
              ),
              Container(
                  child: Container(
                height: 250.0,
                child: Column(
                  children: _group
                      .map((t) => RadioListTile(
                            title: Text("${t.text}"),
                            groupValue: _currVal,
                            value: t.index,
                            onChanged: (val) {
                              setState(() {
                                print(
                                    "Print Current Value====" + val.toString());
                                _currVal = val;
                                /*print("Print Current Value2====" +
                                    val.index.toString());
                                _currVal = val.index;
                                _currText = t.text;*/
                              });
                            },
                          ))
                      .toList(),
                ),
              )),
              Container(
                width: 10,
                child: RaisedButton(
                  child: Text("On Pressed"),
                  onPressed: () {
                    widget.onPressed();
                    //getItems();
                    var objToSend = {
                      for (int i = 0; i < widget.dynamicJsonField.length; i++)
                        if (widget.dynamicJsonField[i]['id'] == "checkBox")
                          if (widget.dynamicJsonField[i]['is_active'])
                            widget.dynamicJsonField[i]['field_id']:
                                widget.dynamicJsonField[i]['is_active']
                    };

                    for (int i = 0; i < widget.dynamicJsonField.length; i++) {
                      if (widget.dynamicJsonField[i]['id'] == "textArea") {
                        print("Text==== " + widget.textAreaController[i].text);
                      }
                    }

                    print("Onj to send === " + objToSend.toString());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onChanged() async {
    setState(() {});
  }
}

Widget widgetPage(String title) {
  if (title == "Feature") {
    return Container(
      child: Text("Test"),
    );
  }
  return Column(
    mainAxisAlignment: MainAxisAlignment.start,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
          child: Text(
        title,
        style: TextStyle(
          fontFamily: FontFamily.Poppins,
          fontSize: 25,
          color: AppColors.APP_COLOR_PRIMARY,
          fontWeight: FontWeight.w700,
        ),
      )),
    ],
  );
}

class GroupModel {
  String text;
  int index;

  GroupModel({this.text, this.index});
}
