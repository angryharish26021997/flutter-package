
import 'package:flutter/material.dart';
import 'package:harish_package/Constants/SvgPictures.dart';
import 'package:harish_package/OnBoardingScreenComponent.dart';

import 'SignUpScreen.dart';
import 'SplashScreen.dart';


class OnBoardingScreen extends StatefulWidget {
  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  List<Widget> items = [
    buildPage("Feature", Colors.red,SvgPictures.NEW_CART_SVG, 'Simplead is a lead Management System,\nLead management is a set of methodologies,\nsystems, and practices designed to generate new\npotential business clientele, generally operated\nthrough a variety of marketing campaigns\nor programs.'),
    buildPage("Advanced", Colors.blue, SvgPictures.NEW_CART_SVG, 'Test2'),
    buildPage("Sales force", Colors.brown, SvgPictures.NEW_CART_SVG, 'Test3'),
    buildPage("Sales force", Colors.brown, SvgPictures.NEW_CART_SVG, 'Test4')
  ];

  @override
  Widget build(BuildContext context) {
    return OnBoardingScreenComponent(
      items: items,
      signInMethod: signInMethod,
      skipMethod: skipMethod,
    );
  }

  Future<void> signInMethod() async {
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => SignUpScreen()));
  }
  Future<void> skipMethod() async {
    /*Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => SignUpScreen()));*/
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => SignUpScreen()));
  }
}
