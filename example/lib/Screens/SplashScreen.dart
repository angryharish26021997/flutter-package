import 'dart:async';
import 'dart:convert';

import 'package:example/Constants/AppColors.dart';
import 'package:example/Constants/ImageAssets.dart';
import 'package:flutter/material.dart';
import 'package:harish_package/SplashScreenComponent.dart';

import 'onBoardingScreen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  int splashDelay = 2;

  Color s;

  @override
  void initState() {
    super.initState();

    //_loadWidget();


  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => OnBoardingScreen()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SplashScreenComponent(
      image: ImageAssets.VISIBLE,
      color: AppColors.APP_COLOR_PRIMARY,
      text: "Aagnia",
      function: onPress,

    ));
    //return Scaffold(body: SplashScreenComponent());
  }

  Color colorConvert(String color) {
    color = color.replaceAll("#", "");
    if (color.length == 6) {
      s = Color(int.parse("0xFF" + color));
      return s;
    } else if (color.length == 8) {
      return Color(int.parse("0x" + color));
    }
  }

  Future<void> onPress() async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => OnBoardingScreen()));
  }
}
