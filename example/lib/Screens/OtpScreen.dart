import 'package:example/Constants/AppColors.dart';
import 'package:example/Constants/AppStrings.dart';
import 'package:flutter/material.dart';
import 'package:harish_package/OtpScreenComponent.dart';
import 'DashBoardScreen.dart';
import 'package:toast/toast.dart';

class OtpScreen extends StatefulWidget {
  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  var _formKey = GlobalKey<FormState>();
  String otpValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: OtpScreenComponent(
        globalKey: _formKey,
        otpSuccess: signInPage,
        backgroundColor: AppColors.APP_COLOR_PRIMARY,
        buttonColor: AppColors.APP_COLOR_PRIMARY,
        title: AppStrings.APP_NAME,
        validateCondition: validationCondition,

      ),
    );
  }

  Future<void> signInPage() async {
    final isValid = _formKey.currentState.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState.save();

    if(otpValue == "99999"){

      Toast.show("Login Succeed", context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);

      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) => DashBoardScreen()));
    }

    else{
      Toast.show(AppStrings.INVALID_OTP, context, duration: Toast.LENGTH_SHORT, gravity:  Toast.BOTTOM);
    }



  }


  Future<dynamic> validationCondition(String val) async {

    setState(() {
      otpValue = val;
    });
  }
}
