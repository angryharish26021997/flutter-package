import 'package:example/Screens/ThemePickerSelect.dart';
import 'package:harish_package/ColorTheme/Third.dart';
import 'package:harish_package/ColorTheme/ThemeColorComponent.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:harish_package/DataModalComponent/SlidableListView.dart';
import 'package:toast/toast.dart';

class ThemePickerScreen extends StatefulWidget {
  @override
  _ThemePickerScreenState createState() => _ThemePickerScreenState();
}

class _ThemePickerScreenState extends State<ThemePickerScreen> {
  List<int> data = List();

  List<String> actions = ["Delete", "Edit"];

  @override
  void initState() {
    super.initState();
    data = List.generate(20, (index) {
      return index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () {
          print("Print Back Pressed");
          Navigator.pop(context, true);
          return;
        },
        child: Scaffold(
          appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            title: Text("Slider"),
          ),
          body: Container(
            margin: EdgeInsets.only(top: 30),
            child: SlideListView(
              itemBuilder: (bc, index) {
                return Container(
                    width: MediaQuery.of(context).size.width,

                    //padding: EdgeInsets.all(10),
                    child: Card(
                      elevation: 10,
                      shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(15.0),
                              bottomRight: Radius.circular(15.0))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              padding: EdgeInsets.all(10),
                              child: Text(
                                'Child ${data[index]}',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    color: Colors.black87),
                              )),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  padding: EdgeInsets.all(10),
                                  child: Text(
                                    'Sub Child ${data[index]}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: Colors.black87),
                                  )),
                              Container(
                                  padding: EdgeInsets.all(10),
                                  child: Text(
                                    'Content ${data[index]}',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 14,
                                        color: Colors.black87),
                                  )),
                            ],
                          ),
                          /* Container(
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  'Sub Child ${data[index]}',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14,
                                      color: Colors.black87),
                                )),
                            Container(
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  'Content ${data[index]}',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      fontSize: 12,
                                      color: Colors.black87),
                                )),*/
                          InkWell(
                            onTap: () {
                              print("Data" + data[index].toString());

                              Toast.show(data[index].toString(), context);
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    shape: BoxShape.rectangle,
                                    borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(15.0),
                                        bottomRight: Radius.circular(15.0))),
                                alignment: Alignment.bottomRight,
                                padding: EdgeInsets.all(10),
                                child: Text(
                                  'View More',
                                  style: TextStyle(
                                      fontWeight: FontWeight.w900,
                                      fontSize: 14,
                                      color: Colors.black54),
                                )),
                          ),
                        ],
                      ),
                    ));
              },
              actionWidgetDelegate: ActionWidgetDelegate(actions.length,
                  (actionIndex, listIndex) {
                print(actions[actionIndex].toString());
                if (actionIndex == 0) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.delete,
                        color: Colors.black,
                      ),
                      Text(
                        actions[0].toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: Colors.black87),
                      ),
                    ],
                  );
                } else {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(
                        Icons.more,
                        color: Colors.black,
                      ),
                      Text(
                        actions[1].toString(),
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 12,
                            color: Colors.black87),
                      ),
                    ],
                  );
                }
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[Container()],
                );
              }, (int indexInList, int index, BaseSlideItem item) {
                if (index == 0) {
                  //item.remove();
                  item.close();
                  //_showMyDialog(indexInList, item);
                } else {
                  item.close();
                  Toast.show("Close this Item", context);
                }
              }, [Colors.white, Colors.white]),
              dataList: data,
              refreshCallback: () async {
                await Future.delayed(Duration(seconds: 2));

                return;
              },
            ),
          ),
        ));
  }

  Future<void> onPress() async {
    Navigator.of(context)
        .push(new MaterialPageRoute(builder: (_) => new ThemePickerSelect()));
  }

  Color appbarColor = Colors.blueGrey;

  Color backgroundColor = Colors.grey.shade200;

  Set<Color> swatches = Colors.primaries.map((e) => Color(e.value)).toSet();

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    int intValue = prefs.getInt('intValue');
    String stringValue = prefs.getString('stringValue');
    print("Init Value====>" + intValue.toString());
    print("Init Value====>" + stringValue.toString());

    int colorCodeInt = int.parse(stringValue);

    setState(() {
      backgroundColor = Color(colorCodeInt);
      this.mounted;
    });

    print("BackGroundColor====>" + backgroundColor.toString());
    return intValue;
  }
}
