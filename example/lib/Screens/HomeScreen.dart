import 'dart:wasm';

import 'package:flutter/material.dart';
import 'package:harish_package/HomeScreenComponent.dart';
import '../Utils/class_builder.dart';
import 'package:kf_drawer/kf_drawer.dart';

class HomeScreen extends KFDrawerContent {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    ClassBuilder.registerClasses();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HomeScreenComponent(
        onMenuPressed : onMenuPressed,
      ),
    );
  }

  Future<void> onMenuPressed() async{
    setState(() {
      widget.onMenuPressed();
    });
  }
}
