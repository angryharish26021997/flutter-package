import 'package:flutter/material.dart';
import 'package:harish_package/ForgotScreenComponent.dart';
import 'package:harish_package/Utils/SystemUtils.dart';
import 'package:example/Screens/SignUpScreen.dart';
import 'package:toast/toast.dart';
class ForgotScreen extends StatefulWidget {
  @override
  _ForgotScreenState createState() => _ForgotScreenState();
}

class _ForgotScreenState extends State<ForgotScreen> {

  final emailController = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return ForgotScreenComponent(signUpFunction: signUpFunction,emailController: emailController,sentFunction: sentFunction,globalKey: _formKey,);
  }

  Future<void> sentFunction() async{
    final isValid = _formKey.currentState.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState.save();

    if(SystemUtils.hasMatchEmail(emailController.text.toString())){

      Toast.show("Password Send Successfully",context);

    }


  }

  Future<void> signUpFunction() async{
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => SignUpScreen()));
  }
}
