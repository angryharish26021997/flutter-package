import 'package:flutter/material.dart';
import 'package:harish_package/DashBoardComponent.dart';
import '../Utils/class_builder.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'DemoScreen.dart';
import 'HomeScreen.dart';
import 'SettingScreen.dart';
import 'SignUpScreen.dart';
import 'package:toast/toast.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  _DashBoardScreenState createState() => _DashBoardScreenState();
}

class _DashBoardScreenState extends State<DashBoardScreen>
    with TickerProviderStateMixin {
  KFDrawerController _drawerController;

  @override
  void initState() {
    ClassBuilder.registerClasses();
    super.initState();

    _drawerController = KFDrawerController(
      initialPage: ClassBuilder.fromString('HomeScreen'),
      items: [
        KFDrawerItem.initWithPage(
          text: Text(
            'Home',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(Icons.settings, color: Colors.white),
          page: HomeScreen(),
        ),
        KFDrawerItem.initWithPage(
          text: Text('Demo', style: TextStyle(color: Colors.white)),
          icon: Icon(Icons.home, color: Colors.white),
          page: DemoScreen(),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            'SETTINGS',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(Icons.settings, color: Colors.white),
          page: SettingScreen(),
        ),

      ],
    );
  }

  Widget myPlatformContainer = Container();

  bool isClicked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(body:isClicked? myPlatformContainer : DashBoardComponent(drawerController: _drawerController,signOutFunction:signOutFunction,bottomBarFunction: bottomBarFunction,));
  }

  Future<void> bottomBarFunction(int index) async{


    setState(() {

      isClicked = false;
    });

    if(index == 0){
      Toast.show("Home Page",context);
      myPlatformContainer = HomeScreen();
    }
    else if(index == 1){
      Toast.show("Demo Page",context);
      myPlatformContainer = DemoScreen();
    }
    else if(index == 2){
      Toast.show("Settings Page",context);
      myPlatformContainer = SettingScreen();
    }
  }

  Future<void> signOutFunction() async{
    Navigator.pushAndRemoveUntil(
        context,
        PageRouteBuilder(pageBuilder: (BuildContext context, Animation animation,
            Animation secondaryAnimation) {
          return SignUpScreen();
        }, transitionsBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation, Widget child) {
          return new SlideTransition(
            position: new Tween<Offset>(
              begin: const Offset(1.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          );
        }),
            (Route route) => false);
  }
}
