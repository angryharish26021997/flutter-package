import 'package:country_code_picker/country_code.dart';
import 'package:example/Constants/AppStrings.dart';
import 'package:example/Screens/DashBoardScreen.dart';
import 'package:example/Screens/OtpScreen.dart';
import 'package:example/Screens/LoginScreen.dart';
import 'package:example/Screens/MultiStageScreen.dart';
import 'package:example/Screens/ThemePickerScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:harish_package/SignUpScreenComponent.dart';
import 'package:toast/toast.dart';
import 'StepperDemo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:harish_package/Constants/AppColors.dart';
class SignUpScreen extends StatefulWidget {
  static String tag = AppStrings.SIGN_UP_TAG;

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final phoneNumberController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();

  List<TabBar> tabWidget = [
    TabBar(tabs: <Widget>[
      Tab(
        text: AppStrings.EMAIL,
      ),
      /* Tab(
        text: AppStrings.EMAIL,
      ),*/
    ])
  ];

  List<String> name = [AppStrings.SOCIAL, AppStrings.MOBILE, AppStrings.EMAIL];

  String phoneNumber;
  String countryCode;
  var _formKey = GlobalKey<FormState>();
  var _phoneGlobalKey = GlobalKey<FormState>();

  //final FirebaseAuth _auth = FirebaseAuth.instance;
  //final GoogleSignIn googleSignIn = GoogleSignIn();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    this.mounted;
    //firbaseInit();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    phoneNumberController.dispose();
    emailController.dispose();
    passwordController.dispose();
    firstNameController.dispose();
    lastNameController.dispose();

    super.dispose();
  }
  Color theme = AppColors.APP_COLOR_PRIMARY;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        print("Back Pressed");
        return;
      },
      child: Scaffold(
        body: SignUpScreenComponent(
          googleSignUp: googleSignUp,
          facebookSignUp: facebookSignUp,
          phoneNumberController: phoneNumberController,
          phoneNumberSignInFunction: phoneNumberSignInFunction,
          globalKey: _formKey,
          phoneGlobalKey: _phoneGlobalKey,
          emailController: emailController,
          passwordController: passwordController,
          firstNameController: firstNameController,
          lastNameController: lastNameController,
          emailPasswordSignInFunction: emailPasswordSignInFunction,
          signInFunction: signInPage,
          tabEnabledName: name,
            colorPicker:colorPicker,
          themeColor: theme,
        ),
      ),
    );
  }


  Future<void> colorPicker() async{

    Navigator.of(context).push(new MaterialPageRoute(builder: (_)=>new ThemePickerScreen()),)
        .then((val)=>val?getStringValuesSF():null);

  }

  getStringValuesSF() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return int
    String stringValue = prefs.getString('stringValue');
    print("Init Value====>"+stringValue.toString());

    int colorCodeInt = int.parse(stringValue);

    setState(() {
      theme = Color(colorCodeInt);
      this.mounted;
    });


   // print("BackGroundColor====>"+backgroundColor.toString());
  }
  Future<void> googleSignUp() async {
    /* final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult = await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    if (user != null) {
      assert(!user.isAnonymous);
      assert(await user.getIdToken() != null);

      final User currentUser = _auth.currentUser;
      assert(user.uid == currentUser.uid);



      return '$user';
    }

    return null;*/

    Toast.show("Google Sign in Method Calling", context);
    Navigator.push(context,
        MaterialPageRoute(builder: (BuildContext context) => StepperDemo()));
  }

  Future<void> facebookSignUp() async {
    Toast.show("Facebook Sign in Method Calling", context);
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => MultiStageScreen()));
  }

  Future<void> emailPasswordSignInFunction() async {
    final isValid = _formKey.currentState.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState.save();
  }

  Future<void> signInPage() async {
    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext context) => LoginScreen()));
  }

  Future<void> phoneNumberSignInFunction() async {
    final isValid = _phoneGlobalKey.currentState.validate();
    if (!isValid) {
      return;
    }
    _phoneGlobalKey.currentState.save();
    phoneNumber = phoneNumberController.text.toString();

    if (phoneNumber.isNotEmpty) {
      if (phoneNumber.length == 10) {
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext context) => OtpScreen()));
      } else {}
    } else {}
  }
}
