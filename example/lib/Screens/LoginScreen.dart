import 'package:flutter/material.dart';
import 'package:harish_package/LoginScreenComponent.dart';
import 'package:example/Screens/ForgotScreen.dart';
import 'package:example/Screens/SignUpScreen.dart';
import 'package:harish_package/Utils/SystemUtils.dart';
import 'package:toast/toast.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final passwordController = TextEditingController();
  final emailController = TextEditingController();
  var _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return LoginScreenComponent(globalKey: _formKey,signInFunction: signInFunction,emailController: emailController,passwordController: passwordController,signUpFunction: signUpFunction,forgotFunction: forgotFunction,);
  }


  Future<void> signInFunction() async{
    final isValid = _formKey.currentState.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState.save();

    if(SystemUtils.hasMatchEmail(emailController.text.toString())){


      Toast.show("Successfully Login",context);

    }
  }
  Future<void> forgotFunction() async{
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => ForgotScreen()));
  }
  Future<void> signUpFunction() async{
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) => SignUpScreen()));
  }
}
