import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:toast/toast.dart';
import 'package:example/Constants/AppColors.dart';
import 'package:harish_package/MultiStageComponent/json_schema.dart';
import 'package:im_stepper/stepper.dart';

class MultiStageScreen extends StatefulWidget {
  @override
  _MultiStageScreenState createState() => _MultiStageScreenState();
}

class _MultiStageScreenState extends State<MultiStageScreen> {
  String form = json.encode({
    //'title': 'Test Form Json Schema',
    //'description': 'My Description',
    'autoValidated': false,
    'fields': [
      {
        'key': 'input1',
        'element': 'email',
        'label': 'Email',
        'placeholder': "Enter Your Username",
        'required': 'True',
        'type': 'email',
        'initialValue' : 'test@gmail.com',
        'readOnly' : true,
      },
      {
        'key': 'password1',
        'element': 'password',
        'label': 'Password',
        'required': true,
        'type': 'password',
        'validator': 'digitsOnly',
        'errMessage' : 'Error',
        'initialValue' : 'Harish',
        'readOnly' : false,
      },
    ]
  });

  static String validationExample(field, value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);

    if (value.isEmpty) {
      return 'Please enter some text';
    }
    if (value.length < 8) {
      return 'Password is too short';
    }

    if (!regExp.hasMatch(value)) {
      return 'Password contains Minimum 1 Upper case Minimum 1 lowercase Minimum 1 Numeric Number Minimum 1 Special Character Common Allow Character ( ! @ # \$ & * ~ )';
    }
    return null;
  }


  dynamic response;

  Map decorations = {
    'input1': InputDecoration(
      prefixIcon: Icon(Icons.email),
      border: OutlineInputBorder(),
    ),
    'userName': InputDecoration(
      prefixIcon: Icon(Icons.person),
      border: OutlineInputBorder(),
    ),
    'phoneNumber': InputDecoration(
      prefixIcon: Icon(Icons.phone_android),
      border: OutlineInputBorder(),
    ),
    'tareatext1': InputDecoration(
      prefixIcon: Icon(Icons.description),
      border: OutlineInputBorder(),
    ),
    'password1': InputDecoration(
        prefixIcon: Icon(Icons.lock), border: OutlineInputBorder()),
  };

  Map validations = {'password1': validationExample};
  Map errorText = {
    'input1': "Error Value",
    'password1': "Enter the Password",
  };

  int activeStep = 5; // Initial step set to 5.

  int upperBound = 6; // upperBound MUST BE total number of icons minus 1.

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.APP_COLOR_PRIMARY,
        title: Text('MultiStage Form'),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: [
            NumberStepper(
              lineLength: MediaQuery.of(context).size.width * 0.1,
              numbers: [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
              ],


              // activeStep property set to activeStep variable defined above.
              activeStep: activeStep,
              // This ensures step-tapping updates the activeStep.
              onStepReached: (index) {
                setState(() {
                  activeStep = index;
                });
              },
            ),
            header(),
            body,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                activeStep != 0 ? previousButton() : Container(),
                nextButton(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  /// Returns the next button.
  Widget nextButton() {
    return RaisedButton(
      color: AppColors.APP_COLOR_PRIMARY,
      onPressed: () {
        // Increment activeStep, when the next button is tapped. However, check for upper bound.
        if (activeStep < upperBound) {
          setState(() {
            activeStep++;
          });
        }
      },
      child: Text('Next',style: TextStyle(color: AppColors.APP_COLOR_WHITE),),
    );
  }

  /// Returns the previous button.
  Widget previousButton() {
    return RaisedButton(
      color: AppColors.APP_COLOR_PRIMARY,
      onPressed: () {
        // Decrement activeStep, when the previous button is tapped. However, check for lower bound i.e., must be greater than 0.
        if (activeStep > 0) {
          setState(() {
            activeStep--;
          });
        }
      },

      child: Text('Prev',style: TextStyle(color: AppColors.APP_COLOR_WHITE),),
    );
  }

  /// Returns the header wrapping the header text.
  Widget header() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.orange,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              headerText(),
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Returns the header text based on the activeStep.
  String headerText() {
    switch (activeStep) {
      case 1:
        return 'Radio';

      case 2:
        return 'Mobile';

      case 3:
        return 'FrontEnd Language';

      case 4:
        return 'Backend Language';

      case 5:
        return 'Gender';

      case 6:
        return 'Switch';

      default:
        return 'Login';
    }
  }

  Widget get body {
    switch (activeStep) {
      case 1:
        return _buildBody;
        break;
      case 2:
        return Expanded(
          child: FittedBox(
            child: Center(
              child: Text('Once Again 2'),
            ),
          ),
        );
        break;

      case 3:
        return Expanded(
          child: FittedBox(
            child: Center(
              child: Text('Once Again3'),
            ),
          ),
        );
        break;

      case 4:
        return Expanded(
          child: FittedBox(
            child: Center(
              child: Text('Once Again4'),
            ),
          ),
        );
        break;

      case 5:
        return Expanded(
          child: FittedBox(
            child: Center(
              child: Text('Once Again5'),
            ),
          ),
        );
        break;

      case 6:
        return Expanded(
          child: FittedBox(
            child: Center(
              child: Text('Once Again6'),
            ),
          ),
        );
        break;

      default:
        return _buildBody;
        break;
    }
  }

  Widget get _buildBody {
    return Form(
      //key: _formkey,
      child: new Center(
        child: new Column(children: <Widget>[

          new JsonSchema(
            calendarColor: AppColors.APP_COLOR_PRIMARY,
            decorations: decorations,
            form: form,
            //validations: validations,
            //errorMessages: errorText,
            onChanged: (dynamic response) {
              this.response = response;
            },
            actionSave: (data) {
              print(data);
            },
            //autovalidateMode: AutovalidateMode.onUserInteraction,
            buttonSave: Visibility(
              visible: true,
              child: new Container(
                height: 40.0,
                width: 20.0,
                color: AppColors.APP_COLOR_PRIMARY,
                child: Center(
                  child: Text("Login",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold)),
                ),
              ),
            ),
          ),
          /*       new JsonFormGenerator(
                form: form,
                onChanged: (dynamic value) {
                  print(value);
                  setState(() {
                    this.response = value;
                  });
                },
              ),
              new RaisedButton(
                  child: new Text('Send'),
                  onPressed: () {
                    if (_formkey.currentState.validate()) {
                      print(this.response.toString());
                    }
                  })*/
        ]),
      ),
    );
  }

  void functionCall() {
    print("Response123====>" + response.toString());
  }
}
