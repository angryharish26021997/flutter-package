import 'dart:wasm';

import 'package:flutter/material.dart';
import 'package:harish_package/SettingScreenComponent.dart';
import '../Utils/class_builder.dart';
import 'package:kf_drawer/kf_drawer.dart';

class SettingScreen extends KFDrawerContent {
  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  @override
  void initState() {
    ClassBuilder.registerClasses();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SettingScreenComponent(
        onMenuPressed : onMenuPressed,
      ),
    );
  }

  Future<void> onMenuPressed() async{
    setState(() {
      widget.onMenuPressed();
    });
  }
}
