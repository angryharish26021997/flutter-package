import 'dart:wasm';

import 'package:flutter/material.dart';
import 'package:harish_package/DemoComponent.dart';
import '../Utils/class_builder.dart';
import 'package:kf_drawer/kf_drawer.dart';

class DemoScreen extends KFDrawerContent {
  @override
  _DemoScreenState createState() => _DemoScreenState();
}

class _DemoScreenState extends State<DemoScreen> {
  @override
  void initState() {
    ClassBuilder.registerClasses();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DemoComponent(
          onMenuPressed : onMenuPressed,
          ),
    );
  }

  Future<void> onMenuPressed() async{
    setState(() {
      widget.onMenuPressed();
    });
  }
}
