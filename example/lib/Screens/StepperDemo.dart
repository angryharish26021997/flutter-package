import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:harish_package/MultiStageComponent/json_schema.dart';
import 'package:example/Constants/AppColors.dart';
import 'package:toast/toast.dart';
import 'package:harish_package/StepperComponent/StepperComponent.dart';

class StepperDemo extends StatefulWidget {
  @override
  _StepperDemoState createState() => _StepperDemoState();
}

class _StepperDemoState extends State<StepperDemo> {
  int _currentStep = 0;
  StepperType stepperType = StepperType.horizontal;

  String form = json.encode({
    //'title': 'Test Form Json Schema',
    //'description': 'My Description',
    'autoValidated': false,
    'fields': [
      {
        'key': 'input1',
        'element': 'email',
        'label': 'Email',
        'placeholder': "Enter Your Mail Id",
        'required': 'True',
        'type': 'email',
        //'readOnly' : true, // not editable default value false.
      },
      {
        'key': 'password1',
        'element': 'password',
        'label': 'Password',
        'placeholder': "Enter Your Password",
        'required': true,
        'type': 'password',
        'validator': 'digitsOnly',
      },

  /*    {
        'key': 'input1',
        'element': 'email',
        'label': 'Email',
        'placeholder': "Enter Your Username",
        'required': 'True',
        'type': 'email',
        'initialValue' : 'test@gmail.com',
        'readOnly' : true,
      },
      {
        'key': 'password1',
        'element': 'password',
        'label': 'Password',
        'required': true,
        'type': 'password',
        'validator': 'digitsOnly',
        'errMessage' : 'Error',

        'readOnly' : true,
      },*/
    ]
  });

  String form1 = json.encode({
    'title': 'Test Form Json Schema 2',
    //'description': 'My Description',
    'autoValidated': false,
    'fields': [
      {
        'key': 'phoneNumber',
        'element': 'input',
        'label': 'Phone Number',
        'placeholder': "Enter Your Phone Number",
        'required': 'True',
        'type': 'phone'
      },
    ]
  });
  String radio = json.encode({
    'title': 'Test Form Json Schema',
    //'description': 'My Description',
    'autoValidated': false,
    'fields': [
      {
        'key': 'radiobutton1',
        'element': 'radioButton',
        'label': 'Future Select',
        'value': 2,
        'items': [
          {
            'label': "product 1",
            'value': 1,
          },
          {
            'label': "product 2",
            'value': 2,
          },
          {
            'label': "product 3",
            'value': 3,
          }
        ]
      },
    ]
  });

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  static String validationExample(field, value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';

    RegExp regExp = new RegExp(pattern);



    if (value.isEmpty) {
      return 'Please enter some text';
    }
    if (value.length < 8) {
      return 'Password is too short';
    }

/*    if (!regExp.hasMatch(value)) {
      return 'Password contains Minimum 1 Upper case Minimum 1 lowercase \n Minimum 1 Numeric Number Minimum 1 Special Character \n Common Allow Character ( ! @ # \$ & * ~ )';
    }*/
    return null;
  }

  dynamic response;

  Map decorations = {
    'input1': InputDecoration(
      prefixIcon: Icon(Icons.email),
      border: OutlineInputBorder(),
    ),
    'userName': InputDecoration(
      prefixIcon: Icon(Icons.person),
      border: OutlineInputBorder(),
    ),
    'phoneNumber': InputDecoration(
      prefixIcon: Icon(Icons.phone_android),
      border: OutlineInputBorder(),
    ),
    'tareatext1': InputDecoration(
      prefixIcon: Icon(Icons.description),
      border: OutlineInputBorder(),
    ),
    'password1': InputDecoration(
        prefixIcon: Icon(Icons.lock), border: OutlineInputBorder()),
  };

  var data;

  Map validations = {'password1': validationExample};
  Map errorText = {
    'input1': "Error Value",
    'password1': "Enter the Password",
    'phoneNumber': "Enter the Phone Number",
  };


  @override
  void dispose() {
    super.dispose();
  }



  List<Step> steps = [
    Step(
      title: Text('Step 1'),
      content:  Text('Hello World!3443'),
      isActive: true,
    ),
    Step(
      title: Text('Step 2'),
      content: Text('Hello World!23'),
      isActive: true,
    ),
    Step(
      title: Text('Step 3'),
      content: Text('Hello World!'),
      state: StepState.complete,
      isActive: true,
    ),
  ];


  @override
  Widget build(BuildContext context) {
  return StepperComponent(steps: steps,);
  }


  Widget get buildBody{
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Flutter Stepper Demo'),
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: Stepper(

                type: stepperType,
                physics: ScrollPhysics(),
                currentStep: _currentStep,
                onStepTapped: (step) => tapped(step),
                onStepContinue: continued,
                onStepCancel: cancel,
                steps: <Step>[
                  Step(
                    title: new Text('Login'),
                    content: new Center(
                      child: new Column(children: <Widget>[
                        new JsonSchema(
                          calendarColor: AppColors.APP_COLOR_PRIMARY,
                          decorations: decorations,
                          form: form,
                          validations: validations,
                          errorMessages: errorText,
                          onChanged: (dynamic response) {
                            this.response = response;
                          },
                          actionSave: (data) {
                            setState(() {
                              this.data = data;
                            });
                            print(data);
                            for(int i=0;i<data['fields'].length;i++){
                              Toast.show(data['fields'][i]['value'].toString(), context);
                            }
                            _currentStep < 1 ? setState(() => _currentStep += 1) : null;
                            print("Print----" + _currentStep.toString());
                          },
                          //autovalidateMode: AutovalidateMode.onUserInteraction,
                          buttonSave: Visibility(
                            visible: true,
                            child: new Container(
                              height: 40.0,
                              width: 20.0,
                              color: AppColors.APP_COLOR_PRIMARY,
                              child: Center(
                                child: Text("Login",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ),
                        ),
                      ]),
                    ),
                    isActive: _currentStep >= 0,
                    state: _currentStep >= 0
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                  Step(
                    title: new Text('Phone'),
                    content: Column(
                      children: <Widget>[
                        new JsonSchema(
                          calendarColor: AppColors.APP_COLOR_PRIMARY,
                          decorations: decorations,
                          form: radio,
                          validations: validations,
                          errorMessages: errorText,
                          onChanged: (dynamic response) {
                            this.response = response;
                          },
                          actionSave: (data) {
                            print(data['fields'][0]['value'].toString());

                            for(int i=0;i<data['fields'].length;i++){
                              Toast.show(data['fields'][i]['value'].toString(), context);
                            }


                          },
                          buttonSave: Visibility(
                            visible: true,
                            child: new Container(
                              height: 40.0,
                              width: 20.0,
                              color: AppColors.APP_COLOR_PRIMARY,
                              child: Center(
                                child: Text("Login",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    isActive: _currentStep >= 0,
                    state: _currentStep >= 1
                        ? StepState.complete
                        : StepState.disabled,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  tapped(int step) {
    setState(() => _currentStep = step);
  }

  continued() {

   /* if(data!=null){

    }
    else{
      Toast.show("Please enter all fields", context);
    }*/

    _currentStep < 1 ? setState(() => _currentStep += 1) : null;
    print("Print----" + _currentStep.toString());
  }

  cancel() {
    _currentStep > 0 ? setState(() => _currentStep -= 1) : null;
    print("Print----" + _currentStep.toString());
  }
}
