import 'dart:async';

import 'package:example/Screens/SignUpScreen.dart';
import 'package:flutter/material.dart';
import 'package:example/Constants/AppColors.dart';


import 'Screens/SplashScreen.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runZoned<Future<void>>(() async {
    runApp(new MyApp());
  });
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {


  Future<String> isLoggedIn() async {
   /* sharedPreferences = await SharedPreferences.getInstance();
    AUTH_TOKEN_VALUE = sharedPreferences.getString(AppStrings.AUTH_TOKEN);
    if (AUTH_TOKEN_VALUE != null) {
      await getPrefs();
    }*/
    await getPrefs();
  }

  final routes = <String, WidgetBuilder>{
    SignUpScreen.tag: (BuildContext context) => SignUpScreen(),
    //MyBaseScreen.tag: (BuildContext context) => MyBaseScreen(),
  };

  static Future getPrefs() async {
  /*  userId = sharedPreferences.getString(AppStrings.USER_ID);
    firstName = sharedPreferences.getString(AppStrings.NAME);
    lastName = sharedPreferences.getString(AppStrings.LAST_NAME);
    LOGIN_ID_VALUE = sharedPreferences.getString(AppStrings.EMAIL);
    phoneNumber = sharedPreferences.getString(AppStrings.PHONE_NUMBER);
    accessToken = sharedPreferences.getString(AppStrings.ACCESS_CODE_TOKEN);
    AUTH_TOKEN_VALUE =
        sharedPreferences.getString(AppStrings.ACCESS_CODE_TOKEN);
    CUSTOMER_TYPE = sharedPreferences.getString(AppStrings.CUSTOMER_TYPE);
    CUSTOMER_TYPE_ID = sharedPreferences.getString(AppStrings.CUSTOMER_TYPE_ID);*/
  }
  @override
  Widget build(BuildContext context) {
   /* return Scaffold(
      body: SplashScreen(),
    );*/


    return new FutureBuilder(
        future: isLoggedIn(),
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: new Container());
            default:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else {
                return MaterialApp(
                  debugShowCheckedModeBanner: false,
                  theme: ThemeData.light().copyWith(
                      primaryColor: Colors.green,
                      primaryColorDark: Colors.green,
                      textTheme: Theme.of(context)
                          .textTheme
                          .apply(fontFamily: 'NotoSans')),
                  home: getHomeWidget(),
                  routes: routes,
                );
              }
          }
        });

  }

  Widget getHomeWidget() {
    /*if (AUTH_TOKEN_VALUE == null) {
      IS_PRE_LOGIN = true;
      return new Login();
    } else {
      IS_PRE_LOGIN = false;
      return new MyBaseScreen();
      //return new MyHomePage();
    }*/

    return SplashScreen();
  }
}
