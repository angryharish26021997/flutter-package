import 'dart:ui';

class AppColors{
  static const Color APP_COLOR_PRIMARY = Color(0xff0F1D40);
  static const Color APP_COLOR_WHITE = Color(0xffffffff);
  static const Color APP_COLOR_BLACK = Color(0xff000000);
  static const Color primaryColor = Color(0xff0f1d40);
  static const Color dotColor = Color(0xff707070);
  static const Color appWhite = Color(0xffffffff);
  static const Color halfWhite = Color(0xffDCDCDC);
  static const Color errorWarning = Color(0xffff4a06);
  static const Color textColor = Color(0xffA9A9A9);
  static const Color FACEBOOK_COLOR = Color(0xff1877F2);
  static const Color APP_GRAY_1 = Color(0xffadadad);
}
//Reference Link
//https://stackoverflow.com/questions/63349525/changing-backgroundcolor-depending-on-variable-in-flutter
/*
class AppColor {
  static const RED = "RED";
  static const GREEN = "GREEN";
  static const BLUE = "BLUE";
  static const DEFAULT = "DEFAULT";

  static const _colorMap = {
    RED: Colors.red,
    GREEN: Colors.green,
    BLUE: Colors.blue,
    DEFAULT: Colors.teal,
  };

  const AppColor._();

  static getColorFor(String color) => _colorMap[color.toUpperCase()] ?? _colorMap[DEFAULT];
}*/
