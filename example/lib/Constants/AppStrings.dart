class AppStrings{


  static final String EMPTY_STRING  = "";
  //screen heading
  static final String SIGN_IN = "SIGN IN";
  static final String SIGN_UP  = "SIGN UP";
  static final String SIGN_UP_SMALL  = "Sign up";
  static final String FORGOT_PASSWORD_TITLE  = "FORGOT PASSWORD";
  static final String RESET_PASSWORD  = "RESET PASSWORD";
  static final String OTP  = "Otp";

  static final String APP_NAME = "AAGNIA";
  static final String EMAIL = "Email";
  static final String PASSWORD = "Password";
  static final String FIRST_NAME = "First Name";
  static final String LAST_NAME = "Last Name";

  static final String NEXT  = "Next";
  static final String ALREADY_HAVE_AN_ACCOUNT  = "Already have an account? ";
  static final String CREATE_AN_ACCOUNT  = "Create an new account? ";
  static final String OR_SIGN_IN_WITH  = "Or Sign in with";
  static final String INVALID_EMAIL  = "Invalid E-Mail. Email must have @ and . style";
  static final String NAME_MUST_BE_ENTER  = "Name must be enter";
  static final String PASSWORD_TOO_SHORT  = "Password too short";
  static final String EMAIL_CANNOT_BE_EMPTY  = "Email cannot be empty";
  static final String MOBILE_CANNOT_BE_EMPTY  = "Mobile cannot be empty";
  static final String PASSWORD_CANNOT_BE_EMPTY  = "Password cannot be empty";
  static final String FIRST_NAME_CANNOT_BE_EMPTY  = "Firstname cannot be empty";
  static final String LAST_NAME_CANNOT_BE_EMPTY  = "Lastname cannot be empty";
  static final String INVALID_EMAIL_OR_PASSWORD  = "Invalid email id or password";
  static final String INVALID_MOBILE_NUMBER  = "Invalid Mobile Number";
  static final String INVALID_OTP  = "Invalid Otp";
  static final String OTP_CANNOT_BE_EMPTY  = "You must enter the otp code";
  static final String PLEASE_COMPLETE_THE_OTP  = "Please complete the code";

  static final String LOGO  = "LOGO";
  static final String VERIFICATION  = "Verification";
  static final String CONFIRM  = "Confirm";
  static final String FORGOT_PASSWORD  = "Forgot Password? ";
  static final String MOBILE  = "Mobile";
  static final String SOCIAL  = "Social";
  static final String SKIP  = "Skip";
  static final String SENT  = "SENT";
  static final String CONFIRM_PASSWORD  = "Confirm Password";
  static final String NEW_PASSWORD  = "New Password";


  static final String FORGOT_PASSWORD_RESET_TEXT  = "To reset your password. Just enter your email address and \n we will send you a password reset link";
  static final String FORGOT_PASSWORD_RESET_TEXT1  = "Please check your email - the reset password \n instruction should be there";




  //Screen Details

  static final String SIGN_UP_TAG = 'sign-up-tag';

}